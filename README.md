# Django Airavata

Build status [![Build status](https://jenkins.levitnet.be/buildStatus/icon?job=Polla)](https://jenkins.levitnet.be/buildStatus/icon?job=Polla)

Docs [![Docs status](https://readthedocs.org/projects/django-polla/badge/?version=latest)](https://readthedocs.org/projects/django-polla/badge/?version=latest)
PyPi [![PyPI version](https://badge.fury.io/py/airavata.svg)](https://badge.fury.io/py/airavata)


Airavata is a mythological white elephant who carries the Hindu god Indra. Airavata has four tusks and seven trunks and is spotless white. [source on Wikipedia](https://en.wikipedia.org/wiki/Airavata).

Airavata is also a Django 1.8+ library that allows you to hosts multiple dynamic sites running on a single Django instance/db.

I have been using a customized version of [dynamicsites](https://bitbucket.org/uysrc/django-dynamicsites/overview) for a while now. But with the new features from Django 1.7 and 1.8, there exists another simpler, and more pythonesque IMHO, way to achieve the same results. Polla is an attempt at that *other* way.


See documentation on [ReadTheDocs](http://django-polla.readthedocs.org/en/latest/)

If you are planning on contributing (or are just curious about our values), please take the time to read the [project's Code of Conduct](COC.md)


Thanks to:

- @ashwoods for the new name :-)
- [Coraline Ada Ehmke](http://where.coraline.codes/) for creating [the contributor covenant](http://contributor-covenant.org/)
